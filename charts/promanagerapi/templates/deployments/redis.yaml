apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "promanager-api.fullname" . }}-redis
  labels:
    app: {{ template "promanager-api.name" . }}-redis
    chart: {{ .Chart.Name }}-{{ .Chart.Version }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  selector:
    matchLabels:
      app: {{ template "promanager-api.name" . }}-redis
      release: {{ .Release.Name }}
  strategy:
    type: {{ .Values.config.updateStrategy }}
  template:
    metadata:
      labels:
        app: {{ template "promanager-api.name" . }}-redis
        release: {{ .Release.Name }}
      {{- if .Values.persistence.velero.enabled }}
      annotations:
        backup.velero.io/backup-volumes: data
      {{- end }}
    spec:
      affinity:
        nodeAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: {{ .Values.persistence.enabled | ternary "91" "89" }}
              preference:
                matchExpressions:
                  - key: application/state
                    operator: In
                    values:
                      - {{ .Values.persistence.enabled | ternary "stateful" "stateless" }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 90
              podAffinityTerm:
                topologyKey: kubernetes.io/hostname
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "promanager-api.name" . }}-redis
      containers:
        - name: {{ template "promanager-api.fullname" . }}-redis
          image: redis:6.0.8-alpine
          imagePullPolicy: {{ .Values.config.imagePullPolicy }}
          resources:
            requests:
              cpu: {{ .Values.config.redis.resources.requests.cpu }}
              memory: {{ .Values.config.redis.resources.requests.memory }}
            limits:
              cpu: {{ .Values.config.redis.resources.limits.cpu }}
              memory: {{ .Values.config.redis.resources.limits.memory }}
          ports:
            - name: container
              containerPort: 6379
          volumeMounts:
            - name: data
              mountPath: /_data
            - name: data
              mountPath: '/data'
              subPath: cache
          env: []
          livenessProbe:
            tcpSocket:
              port: container
{{ toYaml .Values.probes.liveness | indent 12 }}
          readinessProbe:
            tcpSocket:
              port: container
{{ toYaml .Values.probes.readiness | indent 12 }}
      volumes:
        - name: data
          {{- if .Values.persistence.enabled }}
          persistentVolumeClaim:
            claimName: {{ .Values.persistence.existingClaim.redis | default (printf "%s-redis" (include "promanager-api.fullname" . )) }}
          {{- else }}
          emptyDir: {}
          {{- end }}
