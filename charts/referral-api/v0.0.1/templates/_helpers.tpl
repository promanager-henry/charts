{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "referral-api.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "referral-api.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate referral api certificate
*/}}
{{- define "referral-api.referral-api-certificate" }}
{{- if (not (empty .Values.ingress.referralApi.certificate)) }}
{{- printf .Values.ingress.referralApi.certificate }}
{{- else }}
{{- printf "%s-referral-api-letsencrypt" (include "referral-api.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate referral api hostname
*/}}
{{- define "referral-api.referral-api-hostname" }}
{{- if (and .Values.config.referralApi.hostname (not (empty .Values.config.referralApi.hostname))) }}
{{- printf .Values.config.referralApi.hostname }}
{{- else }}
{{- if .Values.ingress.referralApi.enabled }}
{{- printf .Values.ingress.referralApi.hostname }}
{{- else }}
{{- printf "%s-referral-api" (include "referral-api.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate referral api base url
*/}}
{{- define "referral-api.referral-api-base-url" }}
{{- if (and .Values.config.referralApi.baseUrl (not (empty .Values.config.referralApi.baseUrl))) }}
{{- printf .Values.config.referralApi.baseUrl }}
{{- else }}
{{- if .Values.ingress.referralApi.enabled }}
{{- $hostname := ((empty (include "referral-api.referral-api-hostname" .)) | ternary .Values.ingress.referralApi.hostname (include "referral-api.referral-api-hostname" .)) }}
{{- $protocol := (.Values.ingress.referralApi.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "referral-api.referral-api-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate postgres url
*/}}
{{- define "referral-api.postgres-url" }}
{{- $postgres := .Values.config.postgres }}
{{- if $postgres.url }}
{{- printf $postgres.url }}
{{- else }}
{{- $credentials := ((or (empty $postgres.username) (empty $postgres.password)) | ternary "" (printf "%s:%s@" $postgres.username $postgres.password)) }}
{{- printf "postgresql://%s%s:%d/%s" $credentials $postgres.host $postgres.port $postgres.database }}
{{- end }}
{{- end }}
