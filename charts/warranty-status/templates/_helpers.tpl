{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "warranty-status.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "warranty-status.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate api certificate
*/}}
{{- define "warranty-status.api-certificate" }}
{{- if (not (empty .Values.ingress.api.certificate)) }}
{{- printf .Values.ingress.api.certificate }}
{{- else }}
{{- printf "%s-api-letsencrypt" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate api hostname
*/}}
{{- define "warranty-status.api-hostname" }}
{{- if (and .Values.config.api.hostname (not (empty .Values.config.api.hostname))) }}
{{- printf .Values.config.api.hostname }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- printf .Values.ingress.api.hostname }}
{{- else }}
{{- printf "%s-api" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate api base url
*/}}
{{- define "warranty-status.api-base-url" }}
{{- if (and .Values.config.api.baseUrl (not (empty .Values.config.api.baseUrl))) }}
{{- printf .Values.config.api.baseUrl }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- $hostname := ((empty (include "warranty-status.api-hostname" .)) | ternary .Values.ingress.api.hostname (include "warranty-status.api-hostname" .)) }}
{{- $protocol := (.Values.ingress.api.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "warranty-status.api-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate dashboard certificate
*/}}
{{- define "warranty-status.dashboard-certificate" }}
{{- if (not (empty .Values.ingress.dashboard.certificate)) }}
{{- printf .Values.ingress.dashboard.certificate }}
{{- else }}
{{- printf "%s-dashboard-letsencrypt" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate dashboard hostname
*/}}
{{- define "warranty-status.dashboard-hostname" }}
{{- if (and .Values.config.dashboard.hostname (not (empty .Values.config.dashboard.hostname))) }}
{{- printf .Values.config.dashboard.hostname }}
{{- else }}
{{- if .Values.ingress.dashboard.enabled }}
{{- printf .Values.ingress.dashboard.hostname }}
{{- else }}
{{- printf "%s-dashboard" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate dashboard base url
*/}}
{{- define "warranty-status.dashboard-base-url" }}
{{- if (and .Values.config.dashboard.baseUrl (not (empty .Values.config.dashboard.baseUrl))) }}
{{- printf .Values.config.dashboard.baseUrl }}
{{- else }}
{{- if .Values.ingress.dashboard.enabled }}
{{- $hostname := ((empty (include "warranty-status.dashboard-hostname" .)) | ternary .Values.ingress.dashboard.hostname (include "warranty-status.dashboard-hostname" .)) }}
{{- $protocol := (.Values.ingress.dashboard.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "warranty-status.dashboard-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate postgres url
*/}}
{{- define "warranty-status.postgres-url" }}
{{- $postgres := .Values.config.postgres }}
{{- if $postgres.url }}
{{- printf $postgres.url }}
{{- else }}
{{- $credentials := ((or (empty $postgres.username) (empty $postgres.password)) | ternary "" (printf "%s:%s@" $postgres.username $postgres.password)) }}
{{- printf "postgresql://%s%s:%s/%s" $credentials $postgres.host $postgres.port $postgres.database }}
{{- end }}
{{- end }}
