{{- if .Values.config.api.deploy }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "henry-angular-chart.fullname" . }}-api
  labels:
    app: {{ template "henry-angular-chart.name" . }}-api
    chart: {{ .Chart.Name }}-{{ .Chart.Version }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  selector:
    matchLabels:
      app: {{ template "henry-angular-chart.name" . }}-api
      release: {{ .Release.Name }}
  strategy:
    type: {{ .Values.config.updateStrategy }}
  template:
    metadata:
      labels:
        app: {{ template "henry-angular-chart.name" . }}-api
        release: {{ .Release.Name }}
    spec:
      affinity:
        nodeAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 89
              preference:
                matchExpressions:
                  - key: application/state
                    operator: In
                    values:
                      - stateless
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 90
              podAffinityTerm:
                topologyKey: kubernetes.io/hostname
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "henry-angular-chart.name" . }}-api
      containers:
        - name: {{ template "henry-angular-chart.fullname" . }}-api
          image: {{ .Values.images.api.repository }}:{{ .Values.images.api.tag }}
          imagePullPolicy: {{ .Values.config.imagePullPolicy }}
          resources:
            requests:
              cpu: {{ .Values.config.api.resources.requests.cpu }}
              memory: {{ .Values.config.api.resources.requests.memory }}
            limits:
              cpu: {{ .Values.config.api.resources.limits.cpu }}
              memory: {{ .Values.config.api.resources.limits.memory }}
          ports:
            - name: container
              containerPort: 8443
          env: 
            - name: NODE_ENV
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_node_env
            - name: KEYCLOAK_REG
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_register
            - name: KEYCLOAK_ACCOUNT
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_account
            - name: KEYCLOAK_CLIENTID
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_client_id
            - name: KEYCLOAK_GRANT_TYPE
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_grant_type
            - name: KEYCLOAK_CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_client_secret
            - name: KEYCLOAK_ADMIN_ACCOUNT
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_admin_account
            - name: KEYCLOAK_ADMIN_GRANT_TYPE
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_admin_grant_type
            - name: KEYCLOAK_ADMIN_CLIENTID
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_admin_client_id
            - name: KEYCLOAK_ADMIN_CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_admin_client_secret
            - name: KEYCLOAK_USER_GRANT_TYPE
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_keycloak_user_grant_type
            - name: MONGO_CONNECTION
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mongo_url
            - name: MONGODB_URI
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mongo_url
            - name: MONGOHQ_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mongo_url
            - name: MONGO_USERNAME
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mongo_username
            - name: MONGO_PASSWORD
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mongo_password
            - name: DB_1_PORT_27017_TCP_ADDR
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_db_1_port_27017_tcp_addr
            - name: PROJECT_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_project_secret
            - name: MONGODB_DEBUG
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mongodb_debug
            - name: APP_TITLE
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_app_title
            - name: SESSION_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_session_secret
            - name: twilioAccountSid
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twilio_account_sid
            - name: twilioAccountAuthToken
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twilio_account_auth_token
            - name: twilioApiKey
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twilio_api_key   
            - name: twilioApiSecret
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twilio_api_secret
            - name: chat_twilioAccountSid
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_chat_twilio_account_sid
            - name: chat_twilioAccountAuthToken
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_chat_twilio_account_auth_token
            - name: chat_twilioApiKey
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_chat_twilio_api_key
            - name: chat_twilioApiSecret
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_chat_twilio_api_secret
            - name: S3THUMBURL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_s3_thumb_url
            - name: GIFURL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_gif_url
            - name: WEBURL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_web_url
            - name: PRESIGNUPURL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_pre_signup_url
            - name: CHAT_MESSAGE_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_chat_message_url
            - name: VOIPNOTIFICATION
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_voip_notification
            - name: VIRALCAM_REFERRAL_LINK
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_viralcam_referral_link
            - name: TWITTER_CONSUMER_KEY
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twitter_consumer_key
            - name: TWITTER_CONSUMER_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twitter_consumer_secret
            - name: TWITTER_ACCESS_TOKEN
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twitter_access_token
            - name: TWITTER_TOKEN_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twitter_token_secret
            - name: TWITTER_OAUTH_CALLBACK_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_twitter_oauth_callback_url 
            - name: PM_FASTCAM_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_fastcam_url
            - name: PM_SNAPCAM_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_snapcam_url
            - name: PM_PROMESSENGER_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_promessenger_url
            - name: PM_SOCIALSCREENRECORDER_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_socialscreenrecorder_url
            - name: PM_QUICKCAM_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_quickcam_url
            - name: PM_AFFILIATECALCULATOR_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_affiliatecalculator_url
            - name: PM_SPEEDCAM_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_speedcam_url
            - name: PM_MYBUSINESSCENTER_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mybusinesscenter_url
            - name: PM_SELFIEMOJI_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_selfiemoji_url
            - name: PM_GAMECENTER_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_gamecenter_url
            - name: PM_PIC2ART_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_pic2art_url
            - name: PM_VIDPLAY_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_vidplay_url
            - name: SUBSCRIPTION_CODE
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_subscription_code
            - name: SUBSCRIPTION_YEAR
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_subscription_year
            - name: CLIENT_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_client_url
            - name: MAILER_FROM
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mailer_from
            - name: MAILER_SERVICE_PROVIDER
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mailer_service_provider
            - name: MAILER_PORT
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mailer_port
            - name: MAILER_EMAIL_ID
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mailer_email_id
            - name: MAILER_PASSWORD
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_mailer_password
            - name: GRAPHQL_ACCOUNT
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_graphql_account
            - name: GRAPHQL_DEFAULT_USERNAME
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_graphql_default_username
            - name: GRAPHQL_DEFAULT_PASSWORD
              valueFrom:
                secretKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_graphql_default_password
            - name: APP_NAME
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_app_name
            - name: APP_WEB_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_app_web_url
            - name: APP_PRIVACY_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_app_privacy_url
            - name: APP_TERMS_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "henry-angular-chart.fullname" . }}
                    key: api_app_terms_url
          livenessProbe:
            httpGet:
              path: /
              port: container
{{ toYaml .Values.probes.liveness | indent 12 }}
          readinessProbe:
            httpGet:
              path: /
              port: container
{{ toYaml .Values.probes.readiness | indent 12 }}
{{- end }}