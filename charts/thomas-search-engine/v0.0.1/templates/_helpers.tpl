{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "thomas-search-engine.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "thomas-search-engine.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate web certificate
*/}}
{{- define "thomas-search-engine.web-certificate" }}
{{- if (not (empty .Values.ingress.web.certificate)) }}
{{- printf .Values.ingress.web.certificate }}
{{- else }}
{{- printf "%s-web-letsencrypt" (include "thomas-search-engine.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate web hostname
*/}}
{{- define "thomas-search-engine.web-hostname" }}
{{- if (and .Values.config.web.hostname (not (empty .Values.config.web.hostname))) }}
{{- printf .Values.config.web.hostname }}
{{- else }}
{{- if .Values.ingress.web.enabled }}
{{- printf .Values.ingress.web.hostname }}
{{- else }}
{{- printf "%s-web" (include "thomas-search-engine.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate web base url
*/}}
{{- define "thomas-search-engine.web-base-url" }}
{{- if (and .Values.config.web.baseUrl (not (empty .Values.config.web.baseUrl))) }}
{{- printf .Values.config.web.baseUrl }}
{{- else }}
{{- if .Values.ingress.web.enabled }}
{{- $hostname := ((empty (include "thomas-search-engine.web-hostname" .)) | ternary .Values.ingress.web.hostname (include "thomas-search-engine.web-hostname" .)) }}
{{- $protocol := (.Values.ingress.web.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "thomas-search-engine.web-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate api certificate
*/}}
{{- define "thomas-search-engine.api-certificate" }}
{{- if (not (empty .Values.ingress.api.certificate)) }}
{{- printf .Values.ingress.api.certificate }}
{{- else }}
{{- printf "%s-api-letsencrypt" (include "thomas-search-engine.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate api hostname
*/}}
{{- define "thomas-search-engine.api-hostname" }}
{{- if (and .Values.config.api.hostname (not (empty .Values.config.api.hostname))) }}
{{- printf .Values.config.api.hostname }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- printf .Values.ingress.api.hostname }}
{{- else }}
{{- printf "%s-api" (include "thomas-search-engine.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate api base url
*/}}
{{- define "thomas-search-engine.api-base-url" }}
{{- if (and .Values.config.api.baseUrl (not (empty .Values.config.api.baseUrl))) }}
{{- printf .Values.config.api.baseUrl }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- $hostname := ((empty (include "thomas-search-engine.api-hostname" .)) | ternary .Values.ingress.api.hostname (include "thomas-search-engine.api-hostname" .)) }}
{{- $protocol := (.Values.ingress.api.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "thomas-search-engine.api-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
