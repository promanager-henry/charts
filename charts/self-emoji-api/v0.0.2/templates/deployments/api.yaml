apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "self-emoji-api.fullname" . }}-api
  labels:
    app: {{ template "self-emoji-api.name" . }}-api
    chart: {{ .Chart.Name }}-{{ .Chart.Version }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  selector:
    matchLabels:
      app: {{ template "self-emoji-api.name" . }}-api
      release: {{ .Release.Name }}
  strategy:
    type: {{ .Values.config.updateStrategy }}
  template:
    metadata:
      labels:
        app: {{ template "self-emoji-api.name" . }}-api
        release: {{ .Release.Name }}
    spec:
      affinity:
        nodeAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 89
              preference:
                matchExpressions:
                  - key: application/state
                    operator: In
                    values:
                      - stateless
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 90
              podAffinityTerm:
                topologyKey: kubernetes.io/hostname
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "self-emoji-api.name" . }}-api
      containers:
        - name: {{ template "self-emoji-api.fullname" . }}-api
          image: {{ .Values.images.api.repository }}:{{ .Values.images.api.tag }}
          imagePullPolicy: {{ .Values.config.imagePullPolicy }}
          resources:
            requests:
              cpu: {{ .Values.config.api.resources.requests.cpu }}
              memory: {{ .Values.config.api.resources.requests.memory }}
            limits:
              cpu: {{ .Values.config.api.resources.limits.cpu }}
              memory: {{ .Values.config.api.resources.limits.memory }}
          ports:
            - name: container
              containerPort: 3000
          env:
            - name: KEYCLOAK_BASE_URL
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: base_url
            - name: KEYCLOAK_REALM
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: realm
            - name: KEYCLOAK_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: secret
            - name: KEYCLOAK_REALM_PUBLIC_KEY
              valueFrom:
                secretKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: realm_public_key
            - name: KEYCLOAK_CLIENT_UNIQUE_ID
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: client_unique_id
            - name: KEYCLOAK_ADMIN_USER
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: admin_user
            - name: KEYCLOAK_ADMIN_PASS
              valueFrom:
                secretKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: admin_pass
            - name: CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: client_secret
            - name: REDIS_HOST
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: redis_host
            - name: POSTGRES_DATABASE
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: postgres_database
            - name: POSTGRES_HOST
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: postgres_host
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: postgres_password
            - name: POSTGRES_PORT
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: postgres_port
            - name: POSTGRES_USERNAME
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: postgres_username
            - name: DEBUG
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: debug
            - name: GRAPHQL_PLAYGROUND
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: graphql_playground
            - name: SWAGGER
              valueFrom:
                configMapKeyRef:
                    name: {{ template "self-emoji-api.fullname" . }}
                    key: swagger
            # - name: POSTGRES_URL
            #   valueFrom:
            #     secretKeyRef:
            #         name: {{ template "self-emoji-api.fullname" . }}
            #         key: postgres_url
          livenessProbe:
            httpGet:
              path: /
              port: container
{{ toYaml .Values.probes.liveness | indent 12 }}
          readinessProbe:
            httpGet:
              path: /
              port: container
{{ toYaml .Values.probes.readiness | indent 12 }}
