{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "maidsaroundtown.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "maidsaroundtown.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate ecommerce certificate
*/}}
{{- define "maidsaroundtown.ecommerce-certificate" }}
{{- if (not (empty .Values.ingress.ecommerce.certificate)) }}
{{- printf .Values.ingress.ecommerce.certificate }}
{{- else }}
{{- printf "%s-ecommerce-letsencrypt" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate ecommerce hostname
*/}}
{{- define "maidsaroundtown.ecommerce-hostname" }}
{{- if (and .Values.config.ecommerce.hostname (not (empty .Values.config.ecommerce.hostname))) }}
{{- printf .Values.config.ecommerce.hostname }}
{{- else }}
{{- if .Values.ingress.ecommerce.enabled }}
{{- printf .Values.ingress.ecommerce.hostname }}
{{- else }}
{{- printf "%s-ecommerce" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate ecommerce base url
*/}}
{{- define "maidsaroundtown.ecommerce-base-url" }}
{{- if (and .Values.config.ecommerce.baseUrl (not (empty .Values.config.ecommerce.baseUrl))) }}
{{- printf .Values.config.ecommerce.baseUrl }}
{{- else }}
{{- if .Values.ingress.ecommerce.enabled }}
{{- $hostname := ((empty (include "maidsaroundtown.ecommerce-hostname" .)) | ternary .Values.ingress.ecommerce.hostname (include "maidsaroundtown.ecommerce-hostname" .)) }}
{{- $protocol := (.Values.ingress.ecommerce.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "maidsaroundtown.ecommerce-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate broomlyadmin certificate
*/}}
{{- define "maidsaroundtown.broomlyadmin-certificate" }}
{{- if (not (empty .Values.ingress.broomlyadmin.certificate)) }}
{{- printf .Values.ingress.broomlyadmin.certificate }}
{{- else }}
{{- printf "%s-broomlyadmin-letsencrypt" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate broomlyadmin hostname
*/}}
{{- define "maidsaroundtown.broomlyadmin-hostname" }}
{{- if (and .Values.config.broomlyadmin.hostname (not (empty .Values.config.broomlyadmin.hostname))) }}
{{- printf .Values.config.broomlyadmin.hostname }}
{{- else }}
{{- if .Values.ingress.broomlyadmin.enabled }}
{{- printf .Values.ingress.broomlyadmin.hostname }}
{{- else }}
{{- printf "%s-broomlyadmin" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate broomlyadmin base url
*/}}
{{- define "maidsaroundtown.broomlyadmin-base-url" }}
{{- if (and .Values.config.broomlyadmin.baseUrl (not (empty .Values.config.broomlyadmin.baseUrl))) }}
{{- printf .Values.config.broomlyadmin.baseUrl }}
{{- else }}
{{- if .Values.ingress.broomlyadmin.enabled }}
{{- $hostname := ((empty (include "maidsaroundtown.broomlyadmin-hostname" .)) | ternary .Values.ingress.broomlyadmin.hostname (include "maidsaroundtown.broomlyadmin-hostname" .)) }}
{{- $protocol := (.Values.ingress.broomlyadmin.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "maidsaroundtown.broomlyadmin-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate broomlypro certificate
*/}}
{{- define "maidsaroundtown.broomlypro-certificate" }}
{{- if (not (empty .Values.ingress.broomlypro.certificate)) }}
{{- printf .Values.ingress.broomlypro.certificate }}
{{- else }}
{{- printf "%s-broomlypro-letsencrypt" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate broomlypro hostname
*/}}
{{- define "maidsaroundtown.broomlypro-hostname" }}
{{- if (and .Values.config.broomlypro.hostname (not (empty .Values.config.broomlypro.hostname))) }}
{{- printf .Values.config.broomlypro.hostname }}
{{- else }}
{{- if .Values.ingress.broomlypro.enabled }}
{{- printf .Values.ingress.broomlypro.hostname }}
{{- else }}
{{- printf "%s-broomlypro" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate broomlypro base url
*/}}
{{- define "maidsaroundtown.broomlypro-base-url" }}
{{- if (and .Values.config.broomlypro.baseUrl (not (empty .Values.config.broomlypro.baseUrl))) }}
{{- printf .Values.config.broomlypro.baseUrl }}
{{- else }}
{{- if .Values.ingress.broomlypro.enabled }}
{{- $hostname := ((empty (include "maidsaroundtown.broomlypro-hostname" .)) | ternary .Values.ingress.broomlypro.hostname (include "maidsaroundtown.broomlypro-hostname" .)) }}
{{- $protocol := (.Values.ingress.broomlypro.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "maidsaroundtown.broomlypro-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate broomlycommerce certificate
*/}}
{{- define "maidsaroundtown.broomlycommerce-certificate" }}
{{- if (not (empty .Values.ingress.broomlycommerce.certificate)) }}
{{- printf .Values.ingress.broomlycommerce.certificate }}
{{- else }}
{{- printf "%s-broomlycommerce-letsencrypt" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate broomlycommerce hostname
*/}}
{{- define "maidsaroundtown.broomlycommerce-hostname" }}
{{- if (and .Values.config.broomlycommerce.hostname (not (empty .Values.config.broomlycommerce.hostname))) }}
{{- printf .Values.config.broomlycommerce.hostname }}
{{- else }}
{{- if .Values.ingress.broomlycommerce.enabled }}
{{- printf .Values.ingress.broomlycommerce.hostname }}
{{- else }}
{{- printf "%s-broomlycommerce" (include "maidsaroundtown.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate broomlycommerce base url
*/}}
{{- define "maidsaroundtown.broomlycommerce-base-url" }}
{{- if (and .Values.config.broomlycommerce.baseUrl (not (empty .Values.config.broomlycommerce.baseUrl))) }}
{{- printf .Values.config.broomlycommerce.baseUrl }}
{{- else }}
{{- if .Values.ingress.broomlycommerce.enabled }}
{{- $hostname := ((empty (include "maidsaroundtown.broomlycommerce-hostname" .)) | ternary .Values.ingress.broomlycommerce.hostname (include "maidsaroundtown.broomlycommerce-hostname" .)) }}
{{- $protocol := (.Values.ingress.broomlycommerce.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "maidsaroundtown.broomlycommerce-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
