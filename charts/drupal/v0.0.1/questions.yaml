categories:
  - Server
questions:
  # Config
  - variable: config.clusterType
    default: rke
    description: ""
    type: enum
    options:
      - aks
      - eks
      - gke
      - k3s
      - rke
    required: true
    label: "cluster type"
    group: Config
  - variable: config.imagePullPolicy
    default: Always
    description: ""
    type: enum
    options:
      - Always
      - IfNotPresent
    required: true
    label: "pull policy"
    group: Config
  - variable: config.updateStrategy
    default: Recreate
    description: ""
    type: enum
    options:
      - Recreate
      - RollingUpdate
      - OnDelete
    required: true
    label: "update strategy"
    group: Config
  - variable: config.drupal.resources.enabled
    default: false
    description: ""
    type: enum
    options:
      - defaults
      - custom
      - false
    required: true
    label: "drupal resources defaults"
    show_subquestion_if: custom
    group: Config
    subquestions:
      - variable: config.drupal.resources.requests.cpu
        default: 100m
        description: ""
        type: string
        required: true
        label: "drupal resources requests cpu"
      - variable: config.drupal.resources.requests.memory
        default: 128Mi
        description: ""
        type: string
        required: true
        label: "drupal resources requests memory"
      - variable: config.drupal.resources.limits.cpu
        default: 500m
        description: ""
        type: string
        required: true
        label: "drupal resources limits cpu"
      - variable: config.drupal.resources.limits.memory
        default: 256Mi
        description: ""
        type: string
        required: true
        label: "drupal resources limits memory"
  # drupal
  - variable: config.drupal.replicas
    default: ""
    description: ""
    type: string
    required: false
    label: replicas
    group: drupal
  - variable: config.drupal.username
    default: ""
    description: ""
    type: string
    required: false
    label: username
    group: drupal
  - variable: config.drupal.profile
    default: ""
    description: ""
    type: string
    required: false
    label: profile
    group: drupal
  - variable: config.drupal.siteEmail
    default: ""
    description: ""
    type: string
    required: true
    label: site email
    group: drupal
  - variable: config.drupal.siteName
    default: ""
    description: ""
    type: string
    required: true
    label: site name
    group: drupal
  - variable: config.drupal.wxtTheme
    default: ""
    description: ""
    type: string
    required: true
    label: wxt theme
    group: drupal
  - variable: config.drupal.version
    default: d9
    description: ""
    type: string
    required: true
    label: version
    group: drupal
  - variable: config.drupal.extraSettings
    default: ""
    description: ""
    type: string
    required: false
    label: extra settings
    group: drupal
  - variable: config.drupal.extraInstallScripts
    default: ""
    description: ""
    type: string
    required: false
    label: extra install scripts
    group: drupal
  - variable: config.drupal.extraUpgradeScripts
    default: ""
    description: ""
    type: string
    required: false
    label: extra upgrade scripts
    group: drupal
  - variable: config.drupal.install
    default: true
    description: ""
    type: boolean
    required: true
    label: install
    group: drupal
  - variable: config.drupal.securityContext.fsGroup
    default: 82
    description: ""
    required: true
    type: string
    label: fs group
    group: drupal
  - variable: config.drupal.securityContext.runAsUser
    default: 82
    description: ""
    required: true
    type: string
    label: run as user
    group: drupal
  - variable: config.drupal.securityContext.runAsGroup
    default: 82
    description: ""
    required: true
    type: string
    label: run as group
    group: drupal
  - variable: config.drupal.smtp.host
    default: mail
    description: ""
    required: true
    type: string
    label: smtp host
    group: drupal
  - variable: config.drupal.smtp.tls
    default: true
    description: ""
    required: true
    type: boolean
    label: smtp tls
    group: drupal
  - variable: config.drupal.smtp.starttls
    default: true
    description: ""
    required: true
    type: boolean
    label: smtp startTls
    group: drupal
  - variable: config.drupal.smtp.auth.enabled
    default: false
    description: ""
    required: true
    type: boolean
    label: smtp auth enabled
    group: drupal
  - variable: config.drupal.smtp.auth.user
    default: ""
    description: ""
    required: false
    type: string
    label: smtp auth user
    group: drupal
  - variable: config.drupal.smtp.auth.password
    default: ""
    description: ""
    required: false
    type: password
    label: smtp auth password
    group: drupal
  - variable: config.drupal.smtp.auth.method
    default: LOGIN
    description: ""
    required: true
    type: string
    label: smtp auth method
    group: drupal
  # postgresql
  - variable: config.postgres.postgresql.enabled
    default: false
    description: ""
    type: boolean
    label: postgres enable
    required: true
    group: postgresql
  - variable: config.postgres.postgresql.image.tag
    default: 11.6.0-debian-9-r0
    description: ""
    required: true
    type: string
    label: image tag
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlUsername
    default: wxt
    description: ""
    type: string
    required: true
    label: username
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlPassword
    default: ""
    description: ""
    required: true
    type: password
    label: password
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlDatabase
    default: ""
    description: ""
    type: string
    required: true
    label: postgresql database
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.listenAddresses
    default: ""
    description: ""
    type: string
    required: true
    label: listen addresses
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.maxConnections
    default: "200"
    description: ""
    type: string
    required: true
    label: max connection
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.sharedBuffers
    default: 512MB
    description: ""
    type: string
    required: true
    label: shared buffers
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.workMem
    default: 2048MB
    description: ""
    type: string
    required: true
    label: work mem
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.effectiveCacheSize
    default: 512MB
    description: ""
    type: string
    required: true
    label: effective cache size
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.maintenanceWorkMem
    default: 32MB
    description: ""
    type: string
    required: true
    label: maintenance work mem
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.minWalSize
    default: 512MB
    description: ""
    type: string
    required: true
    label: minwal size
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.maxWalSize
    default: 512MB
    description: ""
    type: string
    required: true
    label: maxwal size
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.walBuffers
    default: 8048kB
    description: ""
    type: string
    required: true
    label: wal buffers
    group: postgresql
  - variable: config.postgres.postgresql.postgresqlConfiguration.byteaOutput
    default: "'escape'"
    description: ""
    type: string
    required: true
    label: bytea outpust
    group: postgresql
  - variable: config.postgres.postgresql.persistence.enabled
    default: true
    description: ""
    type: string
    required: true
    label: persistence enabled
    group: postgresql
  - variable: config.postgres.postgresql.persistence.size
    default: 50Gi
    description: ""
    type: string
    required: true
    label: persistence size
    group: postgresql
  - variable: config.postgres.postgresql.volumePermissions.enabled
    default: true
    description: ""
    type: boolean
    required: true
    label: volume permissions enabled
    group: postgresql
  - variable: config.postgres.postgresql.service.port
    default: 5432
    description: ""
    type: string
    required: true
    label: service port
    group: postgresql
  # Services and Load Balancing
  - variable: ingress.drupal.enabled
    default: true
    description: ""
    type: boolean
    label: "drupal ingress enabled"
    show_subquestion_if: true
    group: "Services and Load Balancing"
    subquestions:
      - variable: ingress.drupal.tls
        default: false
        description: ""
        type: boolean
        required: true
        label: "drupal ingress tls"
      - variable: ingress.drupal.hostname
        default: xip.io
        description: ""
        type: hostname
        required: true
        label: "drupal ingress hostname"
      - variable: ingress.drupal.issuer.name
        default: letsencrypt-staging
        description: ""
        type: enum
        show_if: ingress.drupal.tls=true
        options:
          - none
          - letsencrypt-cloudflare-staging
          - letsencrypt-cloudflare-prod
          - letsencrypt-staging
          - letsencrypt-prod
        required: true
        label: "drupal ingress issuer name"
      - variable: ingress.drupal.certificate
        default: ""
        description: ""
        type: string
        show_if: ingress.drupal.tls=true&&ingress.drupal.issuer.name=none
        required: false
        label: "drupal ingress certificate"
  - variable: service.drupal.type
    default: ClusterIP
    description: ""
    type: enum
    show_if: ingress.drupal.enabled=false
    options:
      - ClusterIP
      - LoadBalancer
      - NodePort
    required: true
    label: "drupal service type"
    group: "Services and Load Balancing"
  - variable: service.drupal.nodePorts.http
    default:
    description: ""
    type: int
    required: false
    min: 30000
    max: 32767
    show_if: service.drupal.type=NodePort&&ingress.drupal.enabled=false
    label: "drupal http node port"
    group: "Services and Load Balancing"
  - variable: service.drupal.lbPorts.http
    default: 80
    description: ""
    type: int
    min: 0
    max: 65535
    required: true
    show_if: service.drupal.type=LoadBalancer&&ingress.drupal.enabled=false
    label: "drupal http lb port"
    group: "Services and Load Balancing"

  # Images
  - variable: images.drupal.repository
    default:
    description: ""
    type: string
    required: true
    label: "drupal repository"
    group: Images
  - variable: images.drupal.tag
    default: drupalwxt/site-wxt
    description: ""
    type: string
    required: true
    label: "drupal tag"
    group: Images
