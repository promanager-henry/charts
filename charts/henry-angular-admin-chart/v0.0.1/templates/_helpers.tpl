{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "henry-angular-admin-chart.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "henry-angular-admin-chart.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate web certificate
*/}}
{{- define "henry-angular-admin-chart.web-certificate" }}
{{- if (not (empty .Values.ingress.web.certificate)) }}
{{- printf .Values.ingress.web.certificate }}
{{- else }}
{{- printf "%s-web-letsencrypt" (include "henry-angular-admin-chart.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate web hostname
*/}}
{{- define "henry-angular-admin-chart.web-hostname" }}
{{- if (and .Values.config.web.hostname (not (empty .Values.config.web.hostname))) }}
{{- printf .Values.config.web.hostname }}
{{- else }}
{{- if .Values.ingress.web.enabled }}
{{- printf .Values.ingress.web.hostname }}
{{- else }}
{{- printf "%s-web" (include "henry-angular-admin-chart.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate web base url
*/}}
{{- define "henry-angular-admin-chart.web-base-url" }}
{{- if (and .Values.config.web.baseUrl (not (empty .Values.config.web.baseUrl))) }}
{{- printf .Values.config.web.baseUrl }}
{{- else }}
{{- if .Values.ingress.web.enabled }}
{{- $hostname := ((empty (include "henry-angular-admin-chart.web-hostname" .)) | ternary .Values.ingress.web.hostname (include "henry-angular-admin-chart.web-hostname" .)) }}
{{- $protocol := (.Values.ingress.web.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "henry-angular-admin-chart.web-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

